package com.sandbox.scheduler;

import com.sandbox.scheduler.model.Category;
import com.sandbox.scheduler.model.Task;

import java.util.Arrays;
import java.util.List;

/**
 * Created by pchurchward on 2017-07-20.
 */
public class App {

    public static void main(String[] args) {
        Task task1 = new Task(0, Category.RED.name());
        Task task2 = new Task(0, Category.BLUE.name());
        Task task3 = new Task(3, Category.GREEN.name());
        Task task4 = new Task(4, Category.GREEN.name());
        Task task5 = new Task(5, Category.RED.name());
        Task task6 = new Task(0, Category.RED.name());
        List<Task> unsorted = Arrays.asList(task1, task2, task3, task4, task5, task6);
        
        //Testing sample ouputs from the question paper
        //Task task1 = new Task(0,Category.RED.name());
        //Task task2 = new Task(0,Category.RED.name());
        //Task task3 = new Task(3,Category.RED.name());
        //Task task4 = new Task(1,Category.GREEN.name());
        //List<Task> unsorted = Arrays.asList(task1, task2, task3, task4);
        
        unsorted.forEach(t -> Scheduler.scheduleTask(t));
        
        Scheduler.printSchedule();
    }
}
