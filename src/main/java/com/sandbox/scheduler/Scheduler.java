/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sandbox.scheduler;

import com.sandbox.scheduler.model.Task;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author ma
 */
public final class Scheduler {

    private static ArrayList<Task> tasks = new ArrayList<Task>();

    public static void scheduleTask(Task t) {
        tasks.add(t);
    }

    public static void printSchedule() {
        //Collections.sort(List<Task> tasks, Comparator<? super Task> c) ;
        Collections.sort(tasks);
        for(Task t : tasks){
            System.out.println(t.toString());
        }
    }
}
