package com.sandbox.scheduler.model;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Created by pchurchward on 2017-07-20.
 */
public class Task implements Comparable<Task>{

    public int urgency = -1;
    public String category;
    public LocalDateTime timestamp = LocalDateTime.now();
    public UUID uuid = UUID.randomUUID();

    public Task(int urgency, String category) {
        this.urgency = urgency;
        this.category = category;
    }

    public String toString() {

       return "[TASK] UUID: " + uuid.toString() + " URGENCY: " + urgency + " CATEGORY: " + category +" TIMESTAMP: " + timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (urgency != task.urgency) return false;
        if (!category.equals(task.category)) return false;
        if (timestamp != null ? !timestamp.equals(task.timestamp) : task.timestamp != null) return false;
        return uuid != null ? uuid.equals(task.uuid) : task.uuid == null;
    }

    @Override
    public int hashCode() {
        int result = urgency;
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0);
        result = 31 * result + (uuid != null ? uuid.hashCode() : 0);
        return result;
    }
    
    //Implement comparable functioin here
    @Override
    public int compareTo(Task o) {
        boolean check;
        check = (o.urgency == this.urgency)?true:false;
        if(!check)
            return (o.urgency < this.urgency)?0:-1;
        check = (Category.valueOf(o.category).getValue() == Category.valueOf(this.category).getValue())?true:false;
        if(!check)
            return (Category.valueOf(o.category).getValue() < Category.valueOf(this.category).getValue())?0:-1;
        check = (o.timestamp.isEqual(this.timestamp))?true:false;
        if (!check)
            return (o.timestamp.isBefore(this.timestamp))?0:-1;
        return 0;
    } 
   
}
