/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sandbox.scheduler;

import com.sandbox.scheduler.model.Task;
import org.junit.Test;
import static org.junit.Assert.*;
import com.sandbox.scheduler.model.Category;

/**
 *
 * @author ma
 */
public class SchedulerTest {
    
    public SchedulerTest() {
    }
    /**
     * Test of scheduleTask method, of class Scheduler.
     */
    @Test
    public void testScheduleTask() {
        System.out.println("scheduleTask");
        //Task t = null;
        Task t = new Task(0, Category.RED.name());
        try{
        Scheduler.scheduleTask(t);
        }catch(Exception e){
        fail("Fail to add a task");
        }
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of printSchedule method, of class Scheduler.
     */
    @Test
    public void testPrintSchedule() {
        System.out.println("printSchedule");
        try{
        Scheduler.printSchedule();
        }catch(Exception e){
        fail("Fail to print the schedule");
        }
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    /*
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }*/
}
