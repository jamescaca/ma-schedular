Firstly, I got a maven pom project obviously and I open the pom file with netbeans.  The project is ready to get built and tested in this case.

Lamda expression is not supported by the default configuration of pom which points to version 1.5.  I include the new configuration and points it to 1.8.

Task is receiving a string in the constructor.  I fix the test case and clean the error.

CompareTo function is required as we are trying to sort a list of objects with different fields.

Schedular file gets the details of sorting and printing feature.  The functions should be static since we are not initializing the schedular in the main entry point.
